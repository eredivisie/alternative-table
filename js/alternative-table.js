// standard table
function standardTable() {
  $.ajax({
    crossDomain: true,
    headers: {
      'X-Auth-Token': 'd45a9dddf79846c6a3ec832f4649fd1f'
    },
    url: 'https://api.football-data.org/v2/competitions/2003/standings',
    dataType: 'json',
    type: 'GET',
  }).done(function(response) {
    var teams = response.standing;
    $.each(teams, function() {
      var team = $(this)[0];
      $("<tr><th>" + team.position + "</th><td>" + team.team.name + "</td><td>" + team.playedGames + "</td><td>" + team.wins + "</td><td>" + team.draws + "</td><td>" + team.losses + "</td><td>" + team.goals + "</td><td>" + team.goalsAgainst + "</td><td>" + team.goalDifference + "</td><td>" + team.points + "</td</tr>").appendTo(".table tbody");
    });
  });
}

// alternative table
function alternativeTable() {
  $.ajax({
    crossDomain: true,
    headers: {
      'X-Auth-Token': 'd45a9dddf79846c6a3ec832f4649fd1f'
    },
    url: 'https://api.football-data.org/v2/competitions/2003/standings',
    dataType: 'json',
    type: 'GET',
  }).done(function(response) {
    var teams = response.standings[0].table;
    var mostPoints = teams[0].points;
    var fewestPoints = teams[17].points;
    for (var i = mostPoints; i >= fewestPoints; i--) {
      $("<tr id='" + i + "'><th class='position'></th><td class='points'>" + i + "</td><td class='teams'></td></tr>").appendTo(".table tbody");
    };
    $.each(teams, function() {
      var team = $(this)[0];
      var netGoals = "negative";
      var positiveGoals = "";
      if (team.goalDifference > 0) {
        netGoals = "positive"
        positiveGoals = "+"
      };
      if (team.goalDifference == 0) {
        netGoals = "equal"
      };
      $("#" + team.points + " .points").css("color", "black");
      $("<div class='team-box'>" + team.team.name + "<span class='goal-difference " + netGoals + "'>" + positiveGoals + team.goalDifference + "</span></div>").appendTo($("#" + team.points + " .teams"))
      $("#" + team.points + " .position").text(team.position);
    });

  });
}