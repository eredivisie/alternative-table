# Alternative Eredivisie table

An alternative table, based on [footballtabl.es](https://footballtabl.es/german-bundesliga/alternative-table/).

## API

Uses the [football-data.org API](http://api.football-data.org/).